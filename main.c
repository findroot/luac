#include <stdio.h>
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

int main(int argc, char *argv[])
{
    char str[]="helloworld!";
	char buff[256];
	int error;
	lua_State *L;

	L = luaL_newstate();//打开lua
	luaL_openlibs(L);//打开lua库
	luaL_dofile(L, "test.lua");//执行lua脚本
	lua_close(L);//关闭lua

    return 0;
}
